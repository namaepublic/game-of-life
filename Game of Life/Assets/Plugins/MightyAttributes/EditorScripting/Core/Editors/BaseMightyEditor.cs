#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace MightyAttributes.Editor
{
    public abstract class BaseMightyEditor : UnityEditor.Editor
    {
        protected MightyInspector inspector;
        protected MightyScene scene;

        #region Unity Events

        private void OnEnable()
        {
            if (MightySettingsServices.Activated) Enable();
        }

        private void OnDisable()
        {
            if (MightySettingsServices.Activated) Disable();
        }

        public override void OnInspectorGUI()
        {
            try
            {
                if (MightySettingsServices.Activated) InspectorGUI();
                else base.OnInspectorGUI();
            }
            catch (Exception ex)
            {
                if (MightyExceptionUtilities.IsExitGUIException(ex)) return;

                if (ex is BaseAbortInspectorGUIException abortException)
                    abortException.OnInspectorAbort(inspector);
                else
                    Debug.LogException(ex);
            }
        }

        #endregion /Unity Events

        #region Core

        protected bool Enable(bool force = false)
        {
            try
            {
                if (!force)
                {
                    switch (target)
                    {
                        case MonoBehaviour monoBehaviour when Selection.activeObject != monoBehaviour.gameObject:
                        case ScriptableObject _ when Selection.activeObject != target:
                            return false;
                    }
                }

                inspector = new MightyInspector();
                inspector.OnEnableMonoScript(target, serializedObject);
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name != "SerializedObjectNotCreatableException")
                    // This is a terrible fix, but it'll be enough until I find what's actually triggering this...
                    // It might have to do with additive scenes or prefabs
                    Debug.LogException(ex);

                return false;
            }

            return true;
        }

        protected void Disable()
        {
            inspector?.OnDisable();
            inspector = null;
            MightyDrawersDatabase.ClearCachesNotOfTypes(typeof(BaseHierarchyAttribute), typeof(BaseReloadScriptsAttribute));
        }

        protected void SceneGUI()
        {
            if (inspector == null || !inspector.HasMightyMembers)
                return;

            if (!inspector.HasMightyMembers) return;

            inspector.ManageSceneMembers();
        }

        protected void InspectorGUI()
        {
            if (inspector == null && !Enable() || !inspector.HasMightyMembers)
            {
                base.OnInspectorGUI();
                return;
            }

            inspector.BeginOnInspectorGUI();
            inspector.ManageMembers(out var valueChanged);

            if (valueChanged)
            {
                serializedObject.ManageValueChanged();
                inspector.ApplyAutoValues();
                inspector.RefreshAllDrawers();
                serializedObject.ManageValueChanged();
            }

            inspector.EndOnInspectorGUI();
        }

        #endregion /Core

        public void ApplyAutoValues()
        {
            if (!MightySettingsServices.Activated) return;

            if (inspector == null && !Enable(true)) return;
            inspector.ApplyAutoValues();
            Disable();
        }
    }
}
#endif