using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class GridModel : ScriptableObject
{
    public delegate void CellUpdateEvent(Cell cell);

    public Vector2Int size;

    private Cell[,] m_cells;

    private CellUpdateEvent m_onCellUpdate;

    private bool firstPhase;
    private readonly HashSet<Vector2Int> m_positionsToUpdate1 = new HashSet<Vector2Int>();
    private readonly HashSet<Vector2Int> m_positionsToUpdate2 = new HashSet<Vector2Int>();

    public void Init(bool random, CellUpdateEvent onCellUpdate)
    {
        var (width, height) = (size.x, size.y);

        m_cells = new Cell[width, height];
        m_onCellUpdate = onCellUpdate;

        firstPhase = true;
        m_positionsToUpdate1.Clear();
        m_positionsToUpdate2.Clear();

        for (var x = 0; x < width; x++)
        for (var y = 0; y < height; y++)
            m_cells[x, y] = new Cell(random && Random.value > .5f, new Vector2Int(x, y), this);
    }

    public bool TryGetCell(Vector2Int position, out Cell cell)
    {
        var (x, y) = (position.x, position.y);
        if (x < 0 || x >= size.x || y < 0 || y >= size.y)
        {
            cell = default;
            return false;
        }

        cell = m_cells[x, y];
        return true;
    }

    public bool IsCellAlive(Vector2Int cellPosition) => TryGetCell(cellPosition, out var cell) && cell.IsAlive;

    public void AddCellToUpdate(Vector2Int cellPosition)
    {
        if (firstPhase) m_positionsToUpdate1.Add(cellPosition);
        else m_positionsToUpdate2.Add(cellPosition);
    }

    public void LiveGeneration()
    {
        if (firstPhase)
        {
            firstPhase = false;
            m_positionsToUpdate2.Clear();

            foreach (var position in m_positionsToUpdate1)
                if (TryGetCell(position, out var cell))
                    cell.LiveGeneration();
        }
        else
        {
            firstPhase = true;
            m_positionsToUpdate1.Clear();

            foreach (var position in m_positionsToUpdate2)
                if (TryGetCell(position, out var cell))
                    cell.LiveGeneration();
        }
    }

    public void Update()
    {
        if (firstPhase)
            foreach (var position in m_positionsToUpdate1)
            {
                if (!TryGetCell(position, out var cell)) continue;

                cell.Update();
                m_onCellUpdate(cell);
            }
        else
            foreach (var position in m_positionsToUpdate2)
            {
                if (!TryGetCell(position, out var cell)) continue;

                cell.Update();
                m_onCellUpdate(cell);
            }
    }

    public void ForceUpdate()
    {
        foreach (var cell in m_cells)
        {
            cell.Update();
            m_onCellUpdate(cell);
        }
    }

    public void SetCellAlive(Vector2Int cellPosition, bool alive)
    {
        if (TryGetCell(cellPosition, out var cell))
            cell.SetAlive(alive);
    }
}