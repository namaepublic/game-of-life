using MightyAttributes;
using UnityEngine;

public class GridClicker : MonoBehaviour
{
    [Required] public new Camera camera;
    [Required] public GridRenderer gridRenderer;
    [Required] public Grid grid;

    private GridModel m_gridModel;

    private void Start()
    {
        m_gridModel = gridRenderer.gridModel;
        grid.cellSize = Vector3.one / gridRenderer.renderer.sprite.pixelsPerUnit;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            m_gridModel.SetCellAlive(GetCellPositionUnderMouse(), true);
            gridRenderer.AskToRender();
        }
        else if (Input.GetMouseButton(1))
        {
            m_gridModel.SetCellAlive(GetCellPositionUnderMouse(), false);
            gridRenderer.AskToRender();
        }
    }

    private Vector2Int GetCellPositionUnderMouse()
    {
        var gridSize = m_gridModel.size;
        var halfWidth = gridSize.x / 2;
        var halfHeight = gridSize.y / 2;

        var mousePosition = Input.mousePosition;
        var mouseWorldPosition = camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, camera.transform.position.z));
        var cellPosition = grid.WorldToCell(new Vector3(mouseWorldPosition.x, mouseWorldPosition.y, 0));

        return new Vector2Int(cellPosition.x + halfWidth, cellPosition.y + halfHeight);
    }

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void OnInspectorGUI() => Start();

    private void OnDrawGizmosSelected()
    {
        if (!m_gridModel) return;

        Gizmos.color = Color.red;

        var gridSize = m_gridModel.size;

        var halfWidth = gridSize.x / 2;
        var halfHeight = gridSize.y / 2;

        for (var x = 0; x < gridSize.x; x++)
        for (var y = 0; y < gridSize.y; y++)
            Gizmos.DrawCube(grid.GetCellCenterWorld(new Vector3Int(x - halfWidth, y - halfHeight, 0)), grid.cellSize / 4f);
    }
#endif
}