using System;
using MightyAttributes;
#if UNITY_EDITOR
using MightyAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;

public class GridRenderer : MonoBehaviour
{
    [Required] public new SpriteRenderer renderer;

    [Required, CustomDrawer("GridModelDrawer")]
    public GridModel gridModel;

    public float cellSize;
    public Color aliveColor, deadColor;

    private GridModel.CellUpdateEvent m_onCellUpdate;

    private Texture2D m_gridTexture;
    private Color[] m_pixels;

    private bool m_shouldRender;

    private void Start() => m_onCellUpdate = RenderCell;

    public void CreateGridTexture(bool random)
    {
        var (width, height) = (gridModel.size.x, gridModel.size.y);

        gridModel.Init(random, m_onCellUpdate);
        m_pixels = new Color[width * height];
        m_gridTexture = new Texture2D(width, height) {filterMode = FilterMode.Point};

        var sprite = Sprite.Create(m_gridTexture, new Rect(0, 0, width, height), new Vector2(.5f, .5f), 100);
        renderer.sprite = sprite;
        renderer.transform.localScale = new Vector3(cellSize, cellSize);

        ForceRender();
    }

    public void AskToRender() => m_shouldRender = true;

    private void ForceRender()
    {
        gridModel.ForceUpdate();

        m_gridTexture.SetPixels(m_pixels);
        m_gridTexture.Apply();
    }

    private void LateUpdate()
    {
        if (!m_shouldRender) return;
        m_shouldRender = false;
        
        gridModel.Update();

        m_gridTexture.SetPixels(m_pixels);
        m_gridTexture.Apply();
    }

    private void RenderCell(Cell cell)
    {
        var gridPosition = cell.GridPosition;
        m_pixels[gridModel.size.x * gridPosition.y + gridPosition.x] = cell.IsAlive ? aliveColor : deadColor;
    }

#if UNITY_EDITOR
    [Button(executeInPlayMode: false), Order(10)]
    private void CreateRandomGrid()
    {
        if (EditorApplication.isPlaying) return;

        Start();
        CreateGridTexture(true);
        AskToRender();
    }

    private void GridModelDrawer(MightySerializedField serializedField)
    {
        EditorGUILayout.BeginHorizontal();

        var property = serializedField.Property;
        EditorGUILayout.ObjectField(property, EditorGUIUtility.TrTextContent(property.displayName));

        if (MightyGUIUtilities.DrawIconButton(IconName.PLUS, GUILayout.Width(30)))
        {
            var asset = ScriptableObject.CreateInstance(serializedField.PropertyType);
            var path = EditorUtility.SaveFilePanel("Create Grid Model", Application.dataPath, "GridModel", "asset");
            AssetDatabase.CreateAsset(asset, FileUtilities.GetAssetsPath(path));

            property.objectReferenceValue = asset;
        }

        EditorGUILayout.EndHorizontal();
    }
#endif
}