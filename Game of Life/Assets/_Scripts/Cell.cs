using UnityEngine;

public class Cell
{
    private readonly GridModel m_gridModel;

    public bool IsAlive { get; private set; }

    public Vector2Int GridPosition { get; }

    private readonly int m_neighboursCount;
    private readonly Vector2Int[] m_neighboursPosition;

    private bool m_surviveGeneration;

    public Cell(bool isAlive, Vector2Int gridPosition, GridModel gridModel)
    {
        GridPosition = gridPosition;
        m_surviveGeneration = IsAlive = isAlive;
        m_gridModel = gridModel;

        #region Populate Neighbours

        var (x, y) = (gridPosition.x, gridPosition.y);
        var (width, height) = (gridModel.size.x, gridModel.size.y);

        if (x == 0) // left
        {
            if (y == 0) // bottom left corner
            {
                // 3 neighbours
                m_neighboursCount = 3;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // top center
                m_neighboursPosition[0] = new Vector2Int(0, 1);

                // top right
                m_neighboursPosition[1] = new Vector2Int(1, 1);

                // middle right
                m_neighboursPosition[2] = new Vector2Int(1, 0);
            }
            else if (y == height - 1) // top left corner
            {
                // 3 neighbours
                m_neighboursCount = 3;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // middle right
                m_neighboursPosition[0] = new Vector2Int(1, y);

                // bottom right
                m_neighboursPosition[1] = new Vector2Int(1, y - 1);

                // bottom center
                m_neighboursPosition[2] = new Vector2Int(0, y - 1);
            }
            else // left border
            {
                // 5 neighbours
                m_neighboursCount = 5;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // top center
                m_neighboursPosition[0] = new Vector2Int(0, y + 1);

                // top right
                m_neighboursPosition[1] = new Vector2Int(1, y + 1);

                // middle right
                m_neighboursPosition[2] = new Vector2Int(1, y);

                // bottom right
                m_neighboursPosition[3] = new Vector2Int(1, y - 1);

                // bottom center
                m_neighboursPosition[4] = new Vector2Int(0, y - 1);
            }
        }
        else if (x == width - 1) // right
        {
            if (y == 0) // bottom right corner
            {
                // 3 neighbours
                m_neighboursCount = 3;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // middle left
                m_neighboursPosition[0] = new Vector2Int(x - 1, 0);

                // top left
                m_neighboursPosition[1] = new Vector2Int(x - 1, 1);

                // top center
                m_neighboursPosition[2] = new Vector2Int(x, 1);
            }
            else if (y == height - 1) // top right corner
            {
                // 3 neighbours
                m_neighboursCount = 3;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // bottom left
                m_neighboursPosition[0] = new Vector2Int(x - 1, y - 1);

                // middle left
                m_neighboursPosition[1] = new Vector2Int(x - 1, y);

                // bottom center
                m_neighboursPosition[2] = new Vector2Int(x, y - 1);
            }
            else // right border
            {
                // 5 neighbours
                m_neighboursCount = 5;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // bottom left
                m_neighboursPosition[0] = new Vector2Int(x - 1, y - 1);

                // middle left
                m_neighboursPosition[1] = new Vector2Int(x - 1, y);

                // top left
                m_neighboursPosition[2] = new Vector2Int(x - 1, y + 1);

                // top center
                m_neighboursPosition[3] = new Vector2Int(x, y + 1);

                // bottom center
                m_neighboursPosition[4] = new Vector2Int(x, y - 1);
            }
        }
        else // center
        {
            if (y == 0) // bottom border
            {
                // 5 neighbours
                m_neighboursCount = 5;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // middle left
                m_neighboursPosition[0] = new Vector2Int(x - 1, 0);

                // top left
                m_neighboursPosition[1] = new Vector2Int(x - 1, 1);

                // top center
                m_neighboursPosition[2] = new Vector2Int(x, 1);

                // top right
                m_neighboursPosition[3] = new Vector2Int(x + 1, 1);

                // middle right
                m_neighboursPosition[4] = new Vector2Int(x + 1, 0);
            }
            else if (y == height - 1) // top border
            {
                // 5 neighbours
                m_neighboursCount = 5;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // bottom left
                m_neighboursPosition[0] = new Vector2Int(x - 1, y - 1);

                // middle left
                m_neighboursPosition[1] = new Vector2Int(x - 1, y);

                // middle right
                m_neighboursPosition[1] = new Vector2Int(x + 1, y);

                // bottom right
                m_neighboursPosition[3] = new Vector2Int(x + 1, y - 1);

                // bottom center
                m_neighboursPosition[4] = new Vector2Int(x, y - 1);
            }
            else // center
            {
                // 8 neighbours
                m_neighboursCount = 8;
                m_neighboursPosition = new Vector2Int[m_neighboursCount];

                // bottom left
                m_neighboursPosition[0] = new Vector2Int(x - 1, y - 1);

                // middle left
                m_neighboursPosition[1] = new Vector2Int(x - 1, y);

                // top left
                m_neighboursPosition[2] = new Vector2Int(x - 1, y + 1);

                // top center
                m_neighboursPosition[3] = new Vector2Int(x, y + 1);

                // top right
                m_neighboursPosition[4] = new Vector2Int(x + 1, y + 1);

                // middle right
                m_neighboursPosition[5] = new Vector2Int(x + 1, y);

                // bottom right
                m_neighboursPosition[6] = new Vector2Int(x + 1, y - 1);

                // bottom center
                m_neighboursPosition[7] = new Vector2Int(x, y - 1);
            }
        }

        #endregion /Populate Neighbours
        
        if (isAlive) AddToUpdate();
    }

    public void LiveGeneration()
    {
        var livingNeighbours = 0;

        for (var i = 0; i < m_neighboursCount; i++)
            if (m_gridModel.IsCellAlive(m_neighboursPosition[i]))
                livingNeighbours++;

        m_surviveGeneration = livingNeighbours == 3 || IsAlive && livingNeighbours == 2;
        
        if (m_surviveGeneration != IsAlive)
            AddToUpdate();
    }

    public void Update() => IsAlive = m_surviveGeneration;

    public void SetAlive(bool alive)
    {
        if (m_surviveGeneration == alive) return;
        
        m_surviveGeneration = alive;
        AddToUpdate();
    }

    private void AddToUpdate()
    {
        m_gridModel.AddCellToUpdate(GridPosition);

        for (var i = 0; i < m_neighboursCount; i++)
            m_gridModel.AddCellToUpdate(m_neighboursPosition[i]);
    }
}